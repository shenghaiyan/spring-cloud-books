package com.wujunshen.schedule;

import com.wujunshen.service.TestClient;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author: frankwoo(吴峻申) <br>
 * @date: 2017/12/25 <br>
 * @time: 21:22 <br>
 * @mail: frank_wjs@hotmail.com <br>
 */
@Component
//开启定时任务
@EnableScheduling
@Slf4j
public class HystrixJob {
    @Resource
    private TestClient testClient;

    @Scheduled(cron = "0/20 * * * * ?")
    public void doJob() {
        try {
            testClient.test();
        } catch (Exception e) {
            log.info("exception message is:{}", ExceptionUtils.getStackTrace(e));
        }
    }
}